type phase = JEU | ATTENTE
type mode = NORMAL | COURSE

type vehicule = {
    nom :string;
    mutable x : float;
    mutable y : float;
    mutable vx : float;
    mutable vy : float;
    mutable angle : float;
}

type objectif = {
    mutable obj_x : float;
    mutable obj_y : float;
}

type obstacle = {
    mutable ob_x : float;
    mutable ob_y : float;
}

type arene = {
    mutex : Mutex.t;
    cond_joueur_present : Condition.t;
    all_outchan : (string,out_channel) Hashtbl.t;
    mode : mode;
    scores : (string,int) Hashtbl.t;
    all_vehicules : (string,vehicule) Hashtbl.t;
    mutable fin_partie : bool;
    mutable joueur_present : bool;
    mutable phase : phase;
    mutable obstacles : obstacle list;
    mutable objectifs : objectif list;
}

