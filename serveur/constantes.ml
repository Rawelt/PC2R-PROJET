let const_l = 1.
let const_h = 1.
let thrustit = 0.001
let server_refresh_tickrate = 45.
let server_tickrate = 30.
let obj_radius = 0.05
let win_cap = 3
let vitesse_limite = 0.0002
let ob_radius = 0.1
let ve_radius = 0.05

let max_connexion = 4
let pi = 4.0 *. atan 1.0
let dist_secure = 0.002


(* ===========================================================
 * Fonctions pour retrouver les position dans le monde torique
 * =========================================================== *)

let get_true_x x =
  if x > const_l then
    if mod_float x (2.*.const_l) > const_l then
      mod_float x (2.*.const_l) -. (2.*.const_l)
    else
      mod_float x (2.*.const_l)
  else
    if x <= -.const_l then
      if mod_float x (2.*.const_l) <= -.const_l then
        mod_float x (2.*.const_l) +. (2.*.const_l)
      else
        mod_float x (2.*.const_l)
    else
      x

let get_true_y y =
  if y > const_h then
    if mod_float y (2.*.const_h) > const_h then
      mod_float y (2.*.const_h) -. (2.*.const_h)
    else
      mod_float y (2.*.const_h)
  else
    if y <= -.const_h then
      if mod_float y (2.*.const_h) <= -.const_h then
        mod_float y (2.*.const_h) +. (2.*.const_h)
      else
        mod_float y (2.*.const_h)
    else
      y
