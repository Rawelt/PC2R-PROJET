open Mytypes

type instr =
  | CONNECT of (string * mode)
  | EXIT of string
  | NEWCOM of (float * float)
  | ENVOI of string
  | PENVOI of (string * string)
  | UNKNOWN

(* parse_coord "X0.000125Y-2.5" => (0.000125,-2.5) *)
let parse_coord str =
  let li = String.split_on_char 'Y' str in
  let x =  float_of_string @@ String.sub (List.nth li 0) 1 (String.length (List.nth li 0) - 1) in
  let y =  float_of_string @@ List.nth li 1 in
  (x,y)


(* parse_comms "A-1.5T4" => (-1.5,4.) *)
let parse_comms str =
  let li = String.split_on_char 'T' str in
  let t = float_of_string @@ List.nth li 1 in
  let a = float_of_string @@ String.sub (List.nth li 0) 1 (String.length (List.nth li 0) - 1) in
  (a,t)


let parse str =
  let rec reforme_msg li acc =
    match li with
    | x::_::[] -> acc^x
    | h::t -> reforme_msg t (acc^h^"/")
    | [] -> acc
  in
  let li = String.split_on_char '/' str in
  match List.nth li 0 with
  | "CONNECT" ->  (match List.nth li 2 with
                   | "COURSE" -> CONNECT(List.nth li 1,COURSE)
                   | _ -> CONNECT(List.nth li 1,NORMAL))
  | "EXIT" -> EXIT(List.nth li 1)
  | "NEWCOM" -> NEWCOM(parse_comms (List.nth li 1))
  | "ENVOI" -> ENVOI(reforme_msg (List.tl li) "")
  | "PENVOI" -> PENVOI(List.nth li 1,reforme_msg (List.tl @@ List.tl li) "")
  | _ -> UNKNOWN
