open Constantes
open Mytypes


(* Cree une arene avec le mode donne *)
let create_arene mode ={
    mutex = Mutex.create ();
    cond_joueur_present = Condition.create ();
    all_outchan = Hashtbl.create max_connexion;
    mode = mode;
    scores = Hashtbl.create max_connexion;
    all_vehicules = Hashtbl.create max_connexion;
    fin_partie = false;
    joueur_present = false;
    phase = ATTENTE;
    obstacles = [];
    objectifs = [];
}


(* Fonction creant un obstacle pas trop proche des obstacle dans des la liste li *)
let rec create_obstacle li =
  let res = {
      ob_x = Random.float (2. *. const_l) -. const_l;
      ob_y = Random.float (2. *. const_h) -. const_h;
    }
  in
  
  (* La fonction va vérifier si l'obstacle n'est pas trop proche des autres *)
  let rec too_near li =
    match li with
    | [] -> false
    | h::t -> if sqrt (((h.ob_x -. res.ob_x)**2.) +. ((h.ob_y -. res.ob_y)**2.)) <=
                   2.*.ob_radius +. dist_secure then
                true
              else
                too_near t
  in

  if too_near li then
    create_obstacle li
  else
    res


(* Creer une liste de n obstacles *)
let add_obstacles arene n =
  let rec loop n acc =
    if n=0 then
      acc
    else
      loop (n-1) ((create_obstacle acc)::acc)
  in
  arene.obstacles <- loop n []


(* Verifie si la position se trouve dans un obstacle de l'arene*)
let is_in_obstacle arene (x,y) =
  let rec loop li =
    match li with
    | [] -> false
    | h::t -> if sqrt (((h.ob_x -. x)**2.) +. ((h.ob_y -. y)**2.)) <=
                   ob_radius then
                true
              else
                loop t
  in
  loop arene.obstacles


(* La fonction va vérifier si le vehicule touche un autre vehicule de l'arene *)
let is_in_vehicule arene vehicule =
  let b = ref false in
  Hashtbl.iter (fun _ v ->
      if v != vehicule && sqrt (((v.x -. vehicule.x)**2.) +. ((v.y -. vehicule.y)**2.))
                          <= ve_radius then
        b := true) arene.all_vehicules;
  !b


(* Creer un vehicule de nom, verifie qu'il ne se trouve pas dans un obstacle ou dans un joueur *)
let rec create_vehicule arene nom =
  let res = {
      nom = nom;
      x = Random.float (2.*.const_l) -. const_l;
      y = Random.float (2.*.const_h) -. const_h;
      vx = 0.;
      vy = 0.;
      angle = 0.;
    }
  in
  
  if is_in_obstacle arene (res.x,res.y) || is_in_vehicule arene res then
    create_vehicule arene nom
  else
    Hashtbl.add arene.all_vehicules nom res


(* Limite la vitesse du vehicule v si sa norme au carre est trop elevee *)
let limite_vitesse v =
  if (v.vx *. v.vx +. v.vy *. v.vy) > vitesse_limite then
    if v.vy <> 0. then(
      let vx_negatif = v.vx < 0. in
      let vy_negatif = v.vy < 0. in
      let k = abs_float (v.vx/.v.vy) in
      let nouv_vx = sqrt (vitesse_limite/.(k*.k +. 1.)) *. k in
      let nouv_vy = sqrt (vitesse_limite/.(k*.k +. 1.)) in
      (if vx_negatif then
         v.vx <- -.nouv_vx
       else
         v.vx <- nouv_vx);
      if vy_negatif then
        v.vy <- -.nouv_vy
      else
        v.vy <- nouv_vy
    )else
      v.vx <- sqrt vitesse_limite


(* Set l'angle et modifie la vitesse du joueur nom *)
let set_angle_vitesse arene nom (a,t) =
  let v = Hashtbl.find arene.all_vehicules nom in
  v.angle <- mod_float (v.angle +. a) (2. *. pi);
  v.vx <- v.vx +. (t *. thrustit *. (cos v.angle));
  v.vy <- v.vy +. (t *. thrustit *. (sin v.angle));
  limite_vitesse v


(* Creer un objectif en s'assurant de ne pas le mettre dans un obstacle *)
let rec create_objectif arene =
  let res = {
      obj_x = Random.float (2. *. const_l) -. const_l;
      obj_y = Random.float (2. *. const_h) -. const_h;
    }
  in
  if is_in_obstacle arene (res.obj_x,res.obj_y) then
    create_objectif arene
  else
    res


(* Cree n objectif *)
let add_objectifs arene n =
  let rec loop n acc =
    if n=0 then
      acc
    else
      loop (n-1) ((create_objectif arene)::acc)
  in
  arene.objectifs <- loop n []


(* Verifie si le  vehicule a cette position se trouve sur l'objectif*)
let is_on_objectif objectif (x,y) = 
  sqrt (((objectif.obj_x -. x)**2.) +. ((objectif.obj_y -. y)**2.)) <= obj_radius


(* Ajoute un nouveau joueur a l'arene *)
let add_joueur arene nom =
  Hashtbl.add arene.scores nom 0;
  if arene.phase = JEU then
    create_vehicule arene nom


(* Enleve le joueur nom de l'arene et de la liste des out channel*)
let remove_joueur arene nom =
  Hashtbl.remove arene.scores nom;
  Hashtbl.remove arene.all_vehicules nom;
  Hashtbl.remove arene.all_outchan nom


(* Initialise le debut de la partie *)
let init_partie arene =
  Hashtbl.iter (fun nom _ ->
      Hashtbl.replace arene.scores nom 0;
      create_vehicule arene nom
    ) arene.scores


(* Supprime les vehicules en fin de partie *)
let reset_partie arene =
  Hashtbl.clear arene.all_vehicules


(* Donne la chaine donnant la position de l'objectif a l'indice indiquee
 * objectif_chaine arene 0 => "X0Y0.5" *)
let objectif_chaine arene indice =
  let objectif = List.nth arene.objectifs indice in
  "X"^(string_of_float objectif.obj_x)^"Y"^(string_of_float objectif.obj_y)


(* Donne la chaine des infos des vehicules (position, vecteur vitesse et angle)
 * vcoord_chaine arene => "riri:X0.000125Y-2.5VX-0.002VY0A1.57|fifi:X0Y0VX0VY0A0" *)
let vcoords_chaine arene =
  let vcoord_chaine vehicule =
  "X"^(string_of_float vehicule.x)^"Y"^(string_of_float vehicule.y)^
    "VX"^(string_of_float vehicule.vx)^"VY"^(string_of_float vehicule.vy)^
      "A"^(string_of_float vehicule.angle)
  in
  let res = Hashtbl.fold (fun nom v acc ->
                acc^nom^":"^(vcoord_chaine v)^"|") arene.all_vehicules ""
  in
  if String.length res = 0 then
    res
  else
    String.sub res 0 (String.length res - 1)


(* Donne la chaine des scores
 * scores_chaine arene => "riri:5|fifi:0|loulou:1" *)
let scores_chaine arene =
  let res = Hashtbl.fold (fun nom score acc ->
                acc^nom^":"^(string_of_int score)^"|") arene.scores ""
  in
  if String.length res = 0 then
    res
  else
    String.sub res 0 (String.length res - 1)


(* Donne la chaine des obstacles
 * ocoords_chaine () => "X0.2Y0|X0Y0|X-0.6212Y-0.554" *)
let ocoords_chaine arene =
  let ocoord_chaine obstacle =
    "X"^(string_of_float obstacle.ob_x)^"Y"^(string_of_float obstacle.ob_y)
  in
  let res = List.fold_left (fun acc obstacle ->
                acc^(ocoord_chaine obstacle)^"|") "" arene.obstacles
  in
  if String.length res = 0 then
    res
  else
    String.sub res 0 (String.length res - 1)


(* Donne la chaine de la phase de jeu *)
let phase_chaine arene =
  if arene.phase = JEU then
    "jeu"
  else
    "attente"


(* Change la position des vehicules en faisant attention aux collisions *)
let move_all arene =
  Hashtbl.iter (fun nom v ->
      if is_in_obstacle arene (v.x,v.y) || is_in_vehicule arene v then(
        v.vx <- -.v.vx;
        v.vy <- -.v.vy
      );
      v.x <- get_true_x (v.x +. v.vx);
      v.y <- get_true_y (v.y +. v.vy)
    ) arene.all_vehicules;
