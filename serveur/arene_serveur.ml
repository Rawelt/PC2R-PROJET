open Constantes
open Mytypes
open Instruction
open Arene

exception FIN

(* ======================
 * LES DIFFERENTES ARENES
 * ====================== *)

let arene_normale = Arene.create_arene NORMAL
let arene_course = Arene.create_arene COURSE



(* ===================
 * CREATION DU SERVEUR
 * =================== *)

(* Cree le serveur *)
let creer_serveur port max_connexion =
  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0
  and addr = Unix.inet_addr_of_string "127.0.0.1" in
  Unix.setsockopt sock Unix.SO_REUSEADDR true;
  Unix.bind sock (Unix.ADDR_INET (addr,port));
  Unix.listen sock max_connexion;
  sock


(* Traitement generique d'un service *)
let serveur_traitement sock service =
  while true do
    let (s,_) = Unix.accept sock in
    let _ = Thread.create service s in
    ()
  done


(* ===============================================
 * FONCTIONS POUR ENVOYER DES MESSAGES AUX CLIENTS
 * =============================================== *)

(* Envoie un msg a tous les joueurs de l'arene *)
let broadcast_message arene msg =
  Hashtbl.iter (fun _ ch -> output_string ch msg;
                            flush ch) arene.all_outchan


(* Envoie un msg a un joueur de l'arene *)
let send_to arene nom msg =
  let ch = Hashtbl.find arene.all_outchan nom in
  output_string ch msg;
  flush ch


(* =========================================
 * TRAITEMENT DES MESSAGES RECUS DES CLIENTS
 * ========================================= *)

(* Traite le message CONNECT *)
let connect_instr arene nom outchan =
  Mutex.lock arene.mutex;

  if Hashtbl.mem arene.all_outchan nom then(
    (* Le nom est deja choisi *)
    output_string outchan "DENIED/\n";
    flush outchan;
    Mutex.unlock arene.mutex;
    false
  )

  else(
    (* On ajoute le joueur, envoie le msg de retour et on previent les autres joueurs *)
    add_joueur arene nom;
    output_string outchan
      ("WELCOME/"^(phase_chaine arene)^"/"^(scores_chaine arene)^"/"^
         (objectif_chaine arene 0)^"/"^(ocoords_chaine arene)^"/\n");
    flush outchan;
    broadcast_message arene ("NEWPLAYER/"^nom^"/\n");
    Hashtbl.add arene.all_outchan nom outchan;
    if Hashtbl.length arene.all_outchan = 1 then(
      (* On reveille le thread de la partie si c'est le 1er joueur *)
      arene.joueur_present <- true;
      Condition.signal arene.cond_joueur_present
    );
    Mutex.unlock arene.mutex;
    true
  )


(* Traite le message EXIT *)
let exit_instr arene nom file_descr =
  Mutex.lock arene.mutex;
  remove_joueur arene nom;
  broadcast_message arene ("PLAYERLEFT/"^nom^"/\n");
  if Hashtbl.length arene.all_outchan = 0 then(
    arene.phase <- ATTENTE;
    arene.joueur_present <- false
  );
  Unix.shutdown file_descr Unix.SHUTDOWN_ALL;
  Mutex.unlock arene.mutex


(* Traite le message NEWCOM *)
let newcom_instr arene nom (a,t) =
  Mutex.lock arene.mutex;
  if arene.phase = JEU then
    set_angle_vitesse arene nom (a,t);
  Mutex.unlock arene.mutex


(* Traite le message ENVOI*)
let envoi_instr arene nom msg =
  Mutex.lock arene.mutex;
  broadcast_message arene @@ "RECEPTION/"^nom^": "^msg^"/\n";
  Mutex.unlock arene.mutex


(* Traite le message PENVOI *)
let penvoi_instr arene dest src msg =
  if src = dest then
    ()
  else(
    Mutex.lock arene.mutex;
    (try
       send_to arene dest @@ "PRECEPTION/From "^src^": "^msg^"/"^src^"/\n";
       send_to arene src @@ "PRECEPTION/To "^dest^": "^msg^"/"^src^"/\n"
     with
     | Not_found -> ());
    Mutex.unlock arene.mutex;
  )



(* ======================
 * COMPORTEMENT DU CLIENT
 * ====================== *)

(* Traitement du client *)
let traitement file_descr =
  let inchan = Unix.in_channel_of_descr file_descr in
  let outchan = Unix.out_channel_of_descr file_descr in
  let arene = ref arene_normale in

  (* Verification du nom de joueur *)
  let b = ref true in
  let nom = ref "" in
  while !b do
    let line = input_line inchan in
    match parse line with
    | CONNECT(joueur,mode) -> (match mode with
                          | COURSE -> arene := arene_course
                          | NORMAL -> arene := arene_normale);
                         if connect_instr !arene joueur outchan then(
                           b := false;
                           nom := joueur
                         )
    | _ -> print_endline @@ "message inconnu pendant la phase de connexion: "^line
  done;

  (* Ecoute les messages du client *)
  try
    while true do
      let line = input_line inchan in
      let instr = Instruction.parse line in
      match instr with
      | EXIT(n) -> if !nom = n then exit_instr !arene n file_descr
      | NEWCOM(a,t) -> newcom_instr !arene !nom (a,t)
      | ENVOI(msg) -> envoi_instr !arene !nom msg
      | PENVOI(dest,msg) -> penvoi_instr !arene dest !nom msg
      | _ -> print_endline @@ "message de protocole inconnu : "^line
    done
  with
  | FIN -> ()


(* =======================
 * COMPORTEMENT DU SERVEUR
 * ======================= *)

(* Verifie si un vehicule est sur l'objectif, fait le bon traitement si oui *)
let check_objectif arene =
  Hashtbl.iter (fun nom v ->
      if arene.phase == JEU then(
        let score = Hashtbl.find arene.scores nom in
        let obj =
          match arene.mode with
          | NORMAL -> List.nth arene.objectifs 0
          | COURSE -> List.nth arene.objectifs score
        in
        if is_on_objectif obj (v.x,v.y) then(
          let new_score = score + 1 in
          Hashtbl.replace arene.scores nom new_score;
          if new_score = win_cap then(
            arene.phase <- ATTENTE;
            arene.fin_partie <- true
          )else(
            match arene.mode with
            | NORMAL -> add_objectifs arene 1;
                        broadcast_message arene @@ "NEWOBJ/"^(objectif_chaine arene 0)^"/"^
                                                     (scores_chaine arene)^"/\n"
            | COURSE -> send_to arene nom @@ "NEWOBJ/"^(objectif_chaine arene new_score)^"/"^
                                               (scores_chaine arene)^"/\n";
                        Hashtbl.iter (fun n s ->
                            if n <> nom then
                              send_to arene n @@
                                "NEWOBJ/"^(objectif_chaine arene s)^"/"^
                                  (scores_chaine arene)^"/\n"
                          ) arene.scores
          )
        )
      )
    ) arene.all_vehicules


(* Thread auxiliaire de la partie pour update 
 * les positions tous les 1/serveur_refresh_tickrate *)
let server_refresh_tickrate arene =
  while true do
    Mutex.lock arene.mutex;
    if arene.phase = ATTENTE then(
      Mutex.unlock arene.mutex;
      Thread.exit ()
    );
    move_all arene;
    check_objectif arene;
    Mutex.unlock arene.mutex;
    Thread.delay (1./.server_refresh_tickrate)
  done


(* Thread qui lance une partie sur l'arene *)
let rec partie arene =
  (* En attente qu'il y ait au moins 1 joueur *)  
  Mutex.lock arene.mutex;
  add_obstacles arene 20;
  if arene.mode = NORMAL then
    add_objectifs arene 1
  else
    add_objectifs arene win_cap;
  while not arene.joueur_present do
    print_endline "En attente de joueurs...";
    Condition.wait arene.cond_joueur_present arene.mutex;
  done;
  Mutex.unlock arene.mutex;

  Thread.delay 5.;

  (* Initialisation de l'arene et envoie de messages aux clients *)
  Mutex.lock arene.mutex;
  if arene.joueur_present then(
    arene.phase <- JEU;
    init_partie arene;
    broadcast_message arene ("SESSION/"^(vcoords_chaine arene)^"/"^(objectif_chaine arene 0)^"/"^
                         (ocoords_chaine arene)^"/\n");
    let _ = Thread.create server_refresh_tickrate arene in
    print_endline "Debut de partie"  
  );
  Mutex.unlock arene.mutex;

  (* Boucles pour l'envoie de TICK tous les 1/server_tickrate *)
  Thread.delay (1./.server_tickrate);
  Mutex.lock arene.mutex;
  while arene.joueur_present && not arene.fin_partie do
    broadcast_message arene ("TICK/"^(vcoords_chaine arene)^"/\n");
    Mutex.unlock arene.mutex;
    Thread.delay (1./.server_tickrate);
    Mutex.lock arene.mutex
  done;
  
  (* Fin de partie *)
  (* On regarde si c'est bien une fin de partie *)
  if arene.fin_partie then(
    broadcast_message arene ("WINNER/"^(scores_chaine arene)^"\n");
    arene.fin_partie <- false
  );
  reset_partie arene;
  Mutex.unlock arene.mutex;
  partie arene


(* ====
 * MAIN
 * ==== *)
let _ =
  if Array.length Sys.argv != 2 then(
    print_endline "./arene_serveur <port>";
    exit 1
  )else
    let port = int_of_string Sys.argv.(1) in
    let sock = creer_serveur port 4 in
    let _ = Thread.create partie arene_normale in
    let _ = Thread.create partie arene_course in
    serveur_traitement sock traitement
