package ui;

import client.Instruction;
import client.Listener;
import client.ServerTickrate;
import elements.Arene;
import elements.Constantes;
import elements.Phase;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class AreneUI extends Application{
	private Arene arene;

	private Thread listener;
	private Thread drawer;
	private Thread serveur_tickrate;

	public AreneUI(Arene arene) {
		this.arene = arene;
	}

	public void start(Stage stg) {
		try {

			//Quand le programme se ferme par force, on avance une message au serveur avant
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					exit();
				}
			});

			//Creation des noeuds javafx
			MenuConnexion menu = new MenuConnexion();
			JeuCanvas jeu_canvas = new JeuCanvas(900, 900,arene);
			SideBar sidebar = new SideBar(arene,400,jeu_canvas.getHeight());
			GridPane root = new GridPane();
			root.setStyle("-fx-background-color: black");
			root.add(jeu_canvas, 0, 0);
			root.add(sidebar, 1, 0);
			Scene scene = new Scene(menu);

			stg.setScene(scene);
			stg.setTitle("Arene");
			stg.show();

			//================================
			//SPECIFICATION DES EVENT HANDLERS
			//================================

			//Connexion au mode normale de l'arene
			menu.getNormalButton().setOnAction(e -> {
				String nom = menu.getTextField().getText();
				if(!nom.equals("")) {
					if(!connect(nom,"NORMAL",sidebar)) {
						menu.showError();
					}else {
						scene.setRoot(root);
						sidebar.getMode().setText("MODE: NORMAL");
						stg.setMaximized(true);
						jeu_canvas.requestFocus();
						drawer = new RefreshTickrate(jeu_canvas, arene);
						listener = new Listener(arene,sidebar);
						serveur_tickrate = new ServerTickrate(arene);
						drawer.start();
						listener.start();
						serveur_tickrate.start();
					}
				}else {
					menu.showError();
				}
			});

			//Connexion au mode course de l'arene
			menu.getCourseButton().setOnAction(e -> {
				String nom = menu.getTextField().getText();
				if(!nom.equals("")) {
					if(!connect(nom,"COURSE",sidebar)) {
						menu.showError();
					}else {
						scene.setRoot(root);
						sidebar.getMode().setText("MODE: COURSE");
						stg.setMaximized(true);
						jeu_canvas.requestFocus();
						drawer = new RefreshTickrate(jeu_canvas, arene);
						listener = new Listener(arene,sidebar);
						serveur_tickrate = new ServerTickrate(arene);
						drawer.start();
						listener.start();
						serveur_tickrate.start();
					}
				}else {
					menu.showError();
				}
			});

			//Gestion des inputs clavier sur le jeu quand il a le focus
			jeu_canvas.setOnKeyPressed(e -> {
				e.consume();

				//La touche ENTER donne le focus sur le chat
				if(e.getCode() == KeyCode.ENTER) {
					sidebar.getInputChat().requestFocus();
				}
				//Les touches flechees font bouger le vaisseau quand le jeu a le focus
				else {
					synchronized(arene) {
						if(arene.getPhase() != Phase.ATTENTE) {
							if(e.getCode() == KeyCode.LEFT) {
								arene.setA(arene.getA() + Constantes.TURNIT);
							} else if(e.getCode() == KeyCode.RIGHT) {
								arene.setA(arene.getA() - Constantes.TURNIT);
							} else if(e.getCode() == KeyCode.UP) {
								arene.setT(arene.getT()+1);
							}
						}
					}
				}
			});

			//Cliquer sur le jeu lui redonne le focus des inputs
			jeu_canvas.setOnMouseClicked(e -> {
				jeu_canvas.requestFocus();
			});

			//Gestion des inputs clavier quand le focus est sur le chat
			sidebar.getInputChat().setOnKeyPressed(e -> {
				//La touche ESCAPE donne le focus sur le jeu
				if(e.getCode() == KeyCode.ESCAPE) {
					e.consume();
					jeu_canvas.requestFocus();
				}
				//La touche ENTER envoie le message tape
				else if(e.getCode() == KeyCode.ENTER) {
					String msg = sidebar.getInputChat().getText();
					if(!msg.equals("")) {
						//Si le texte commence par <joueur> , il s'agit d'un message prive 
						if(estMessagePrive(msg)){
							int index = msg.indexOf('>');
							synchronized(arene) {
								arene.getOutChan().print("PENVOI/" + msg.substring(1,index) + "/" + msg.substring(index+2, msg.length()) + "/\n");
								arene.getOutChan().flush();
							}
						}else {
							synchronized(arene) {
								arene.getOutChan().print("ENVOI/" + msg + "/\n");
								arene.getOutChan().flush();
							}
						}
						sidebar.getInputChat().setText("");
					}
				}
			});

			//Si on ferme la fenetre graphique, on envoie le message EXIT au serveur
			stg.setOnCloseRequest(e -> {
				exit();
			});
		}catch(Exception e) {
			System.out.println("AreneUI : " + e);
		}
	}

	//Essaie de se connecter a l'arene du mode specifie
	private boolean connect(String nom,String mode,SideBar sidebar) {
		try {
			arene.getOutChan().print("CONNECT/"+nom+"/"+mode+"/\n");
			arene.getOutChan().flush();
			arene.setMonNom(nom);
			return Instruction.parse(arene, arene.getInChan().readLine(),sidebar);
		}catch(Exception e) {
			System.exit(1);
			return false;
		}
	}

	private boolean estMessagePrive(String message) {
		if(message.charAt(0) == '<') {
			int index = message.indexOf('>');
			if(index != -1 && message.length() > index + 2 && message.charAt(index+1) == ' '){
				String joueur = message.substring(1, index);
				if(arene.getScores().containsKey(joueur)) {
					return true;
				}
			}
		}
		return false;
	}

	private void exit() {
		arene.getOutChan().print("EXIT/"+arene.getMonNom()+"/\n");
		arene.getOutChan().flush();
		if(listener != null) {
			listener.interrupt();
		}
		if(drawer != null) {
			drawer.interrupt();
		}
		if(serveur_tickrate != null) {
			serveur_tickrate.interrupt();
		}
	}
}
