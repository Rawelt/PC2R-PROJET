package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class MenuConnexion extends VBox {
	private TextField tf;
	private Label error;
	private Button normal;
	private Button course;
	private boolean error_shown;
	private static int MAX = 15;//Nombre maximum de caracteres pour un nom

	//Il s'agit de la petite fenetre qui demande le nom et le mode de jeu
	public MenuConnexion() {
		super();
		error_shown = false;
		tf = new TextField();
		error = new Label();
		normal = new Button("Normal");
		course = new Button("Course");

		Label label = new Label("Nom : ");
		label.setTextFill(Color.WHITE);
		label.setStyle("-fx-font-weight:bold;"
				+ "-fx-font-size:20px");

		error.setTextFill(Color.CRIMSON);
		error.setPadding(new Insets(10,10,10,10));
		error.setStyle("-fx-font-weight:bold;"
				+ "-fx-font-size:15px;");

		tf.textProperty().addListener((obs,o,n) ->{
			if(n.length()>MAX) {
				tf.setText(n.substring(0, MAX));
			}
		});
		tf.setStyle("-fx-background-color: black;"
				+ "-fx-border-color: white;"
				+ "-fx-border-radius: 2px;"
				+ "-fx-border-width:8px;"
				+ "-fx-text-inner-color:white;"
				+ "-fx-font-size:20px;");

		normal.setMinSize(150, 50);
		normal.setMaxSize(150, 50);
		normal.setFont(Font.font("", FontWeight.BOLD, 20));

		course.setMinSize(150, 50);
		course.setMaxSize(150, 50);
		course.setFont(Font.font("", FontWeight.BOLD, 20));


		HBox nom = new HBox(label,tf);
		nom.setAlignment(Pos.CENTER);
		nom.setPadding(new Insets(10,10,10,10));

		HBox mode = new HBox(normal,course);
		mode.setAlignment(Pos.CENTER);
		mode.setPadding(new Insets(10,10,10,10));

		this.getChildren().addAll(nom,mode);
		this.setPadding(new Insets(20,20,20,20));
		this.setMinHeight(200);
		this.setAlignment(Pos.CENTER);
		this.setStyle("-fx-background-color: black");
	}

	//Affiche un message lorsqu'il y a un probleme a la connexion
	public void showError() {
		if(!error_shown) {
			this.getChildren().add(1, error);
			error_shown = true;
		}
		if(tf.getText().equals("")) {
			error.setText("Il faut entrer un nom");
		}else {
			error.setText("Le nom est déjà pris");
		}
	}


	//=======
	//GETTERS
	//=======

	public TextField getTextField() {
		return tf;
	}

	public Button getNormalButton() {
		return normal;
	}

	public Button getCourseButton() {
		return course;
	}
}
