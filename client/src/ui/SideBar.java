package ui;

import java.util.HashMap;
import java.util.Map;

import elements.Arene;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class SideBar extends VBox{
	private static int MAX_MSG = 30;
	private Map<String,Label> scores; 
	private Arene arene;

	private Label mode;
	private VBox box_scores;
	private ScrollPane box_chat;

	private VBox chat_messages;
	private TextField input_chat;

	public SideBar(Arene arene, double width, double height) {
		this.arene = arene;
		scores = new HashMap<>();
		mode = new Label();
		box_scores = new VBox();
		chat_messages = new VBox();
		input_chat = new TextField();


		mode.setTextFill(Color.WHITE);
		mode.setPadding(new Insets(9,0,0,0));
		mode.setMinSize(width, height/20);
		mode.setMaxSize(width, height/20);
		mode.setAlignment(Pos.CENTER);
		mode.setStyle("-fx-font-size:15px;"
				+ "-fx-font-weight:bold;");

		//==========================
		//Partie affichage du scores
		//==========================

		Label score_title = new Label("SCORES");
		score_title.setTextFill(Color.WHITE);
		score_title.setPadding(new Insets(10,10,10,10));
		score_title.setStyle("-fx-font-weight:bold;"
				+ "-fx-font-size:15px;"
				+ "-fx-border-color:white;"
				+ "-fx-border-width:5px;"
				+ "-fx-border-radius:2px;");

		box_scores.setMinSize(width, 9*height/20);
		box_scores.setMaxSize(width, 9*height/20);
		box_scores.setAlignment(Pos.CENTER);
		box_scores.getChildren().add(score_title);
		box_scores.setStyle("-fx-border-color:white;"
				+ "-fx-border-width:5px;");


		//========================
		//Partie affichage du chat
		//========================

		input_chat.setMinSize(width, height/20);
		input_chat.setMaxSize(width, height/20);
		input_chat.setStyle("-fx-background-color: black;"
				+ "-fx-border-color: white;"
				+ "-fx-border-width:5px;"
				+ "-fx-text-inner-color:white;"
				+ "-fx-font-size:15px;"
				+ "-fx-font-weight:bold;");

		chat_messages.setPadding(new Insets(20,20,20,20));

		box_chat = new ScrollPane(chat_messages);
		box_chat.setFitToWidth(true);
		box_chat.setHbarPolicy(ScrollBarPolicy.NEVER);
		box_chat.setMinHeight(9*height/20);
		box_chat.setStyle("-fx-background:black;"
				+ "-fx-border-color:white;"
				+ "-fx-border-width:5px;");
		box_chat.focusedProperty().addListener((ob,old_val,new_val) -> {
			if(new_val) {
				input_chat.requestFocus();
			}
		});

		VBox chat_container = new VBox();
		chat_container.setMinSize(width, height/2);
		chat_container.setMaxSize(width, height/2);
		chat_container.setAlignment(Pos.CENTER);
		chat_container.getChildren().addAll(box_chat,input_chat);
		chat_container.setStyle("-fx-border-color:white;"
				+ "-fx-border-width:5px;");

		//======================
		//Assemblage du side bar
		//======================

		this.setAlignment(Pos.CENTER);
		this.setStyle("-fx-border-color:white;"
				+ "-fx-border-width:5px;"
				+ "-fx-border-radius:2px;");
		this.setMinSize(width, height);
		this.setMaxSize(width, height);
		this.getChildren().addAll(mode,box_scores,chat_container);
		reception("Pour envoyer un message privé, <nom du joueur>, suivi d'un espace puis du message.");
	}


	//=======
	//GETTERS
	//=======

	public Label getMode() {
		return mode;
	}

	public TextField getInputChat() {
		return input_chat;
	}


	//============================
	//MODIFICATIONS DE L'AFFICHAGE
	//============================

	//Change les scores affiches par les nouvelles
	public void setScores(String scores_chaine) {
		String[] li = scores_chaine.split("\\|");
		synchronized(scores) {
			for(int i=0;i<li.length;i++) {
				String[] user_score = li[i].split(":");
				Label l = scores.get(user_score[0]);
				if(l == null) {
					l = new Label(user_score[0] + " : " + user_score[1]);
					l.setPadding(new Insets(5,5,5,5));
					if(user_score[0].equals(arene.getMonNom())) {
						l.setTextFill(Color.YELLOW);
					}else {
						l.setTextFill(Color.WHITE);
					}
					l.setStyle("-fx-font-size:15px;"
							+ "-fx-font-weight:bold;");
					box_scores.getChildren().add(l);
					scores.put(user_score[0],l);
				}else {
					l.setText(user_score[0] + " : " + user_score[1]);
				}
			}
		}
	}

	//Affiche le nouveau joueur dans la liste des scores
	public void newPlayer(String nom) {
		synchronized(scores) {
			Label l = new Label(nom + " : 0");
			l.setPadding(new Insets(5,5,5,5));
			l.setTextFill(Color.WHITE);
			l.setStyle("-fx-font-size:15px;"
					+ "-fx-font-weight:bold;");
			box_scores.getChildren().add(l);
			scores.put(nom, l);
		}
	}

	//Enleve le joueur de la liste des scores
	public void playerLeft(String nom) {
		synchronized(scores) {
			Label l = scores.get(nom);
			box_scores.getChildren().remove(l);
			scores.remove(nom);
		}
	}

	//Remet la liste des scores a 0
	public void reset() {
		synchronized(scores) {
			for(Map.Entry<String, Label> entry : scores.entrySet()) {
				entry.getValue().setText(entry.getKey() + " : 0");
			}
		}
	}

	//Ajoute le nouveau message dans la chatbox
	public void reception(String message) {
		Label l = new Label(message);
		l.setPrefWidth(Double.MAX_VALUE);
		l.setWrapText(true);
		l.setTextFill(Color.WHITE);
		l.setPadding(new Insets(0, 0, 10, 0));
		l.setStyle("-fx-font-size:13px;"
				+ "-fx-font-weight:bold;");
		synchronized(box_chat) {
			chat_messages.getChildren().add(l);
			if(chat_messages.getChildren().size() > MAX_MSG) {
				chat_messages.getChildren().remove(0);
			}
			box_chat.setVvalue(box_chat.getVmax());
		}
	}

	//Ajoute le nouveau message privee dans la chatbox
	public void preception(String message) {
		Label l = new Label(message);
		l.setPrefWidth(Double.MAX_VALUE);
		l.setWrapText(true);
		l.setTextFill(Color.DEEPSKYBLUE);
		l.setPadding(new Insets(0, 0, 10, 0));
		l.setStyle("-fx-font-size:13px;"
				+ "-fx-font-weight:bold;");
		synchronized(box_chat) {
			chat_messages.getChildren().add(l);
			if(chat_messages.getChildren().size() > MAX_MSG) {
				chat_messages.getChildren().remove(0);
			}
			box_chat.setVvalue(box_chat.getVmax());
		}
	}
}
