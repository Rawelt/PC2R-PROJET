package ui;

import elements.Arene;
import elements.Constantes;
import elements.Objectif;
import elements.Obstacle;
import elements.Vehicule;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class JeuCanvas extends Canvas{
	private Arene arene;

	public JeuCanvas(double width, double height, Arene arene) {
		super(width,height);
		this.arene = arene;
		this.setStyle("-fx-border-color:white;"
				+ "-fx-border-width:8px;"
				+ "-fx-border-radius:2px;");
		getGraphicsContext2D().setFont(new Font(25));;
	}

	//Convertir la coordonnee x de l'arene en la bonne coordonnee x sur le canvas
	private double convertX(double x) {
		double res = x + Constantes.L;
		res = (res/(2.*Constantes.L)) * getWidth();
		return res;
	}

	//Convertir la coordonnee y de l'arene en la bonne coordonnee y sur le canvas
	private double convertY(double y) {
		double res = y + Constantes.H;
		res = (res/(2.*Constantes.H)) * getHeight();
		return getHeight() - res;
	}

	//Dessine les limites de l'arene
	private void drawLimite() {
		GraphicsContext gc = getGraphicsContext2D();
		gc.setStroke(Color.WHITE);
		gc.setLineWidth(8);
		gc.strokeRoundRect(0, 0, getWidth()-1, getHeight()-1,20,20);
	}

	private void drawVehicules() {
		GraphicsContext gc = getGraphicsContext2D();
		double x,x1,x2,x3,x4,y,y1,y2,y3,y4,angle;
		double taille = Constantes.VE_RADIUS * getWidth() / (2*Constantes.L);
		for (Vehicule v : arene.getAllVehicules().values()) {
			x = convertX(v.getX());
			y = convertY(v.getY());
			angle = v.getAngle();

			x1 = x + taille  * Math.cos(angle);
			y1 = y - taille  * Math.sin(angle);
			x2 = x + taille  * Math.cos(angle + Math.PI/5 + Math.PI);
			y2 = y - taille  * Math.sin(angle + Math.PI/5 + Math.PI);
			x3 = x + (taille/2)  * Math.cos(angle + Math.PI);
			y3 = y - (taille/2)  * Math.sin(angle + Math.PI);
			x4 = x + taille  * Math.cos(angle - Math.PI/5 + Math.PI);
			y4 = y - taille  * Math.sin(angle - Math.PI/5 + Math.PI);
			double[] xArray = {x1,x2,x3,x4};
			double[] yArray = {y1,y2,y3,y4};
			if(v == arene.getMonVehicule()) {
				gc.setFill(Color.YELLOW);
			}else {
				gc.setFill(Color.WHITE);
			}
			gc.fillPolygon(xArray, yArray, 4);
			gc.setFill(Color.RED);
			gc.fillOval(x-2, y-2, 4, 4);
		}
	}

	private void drawObjectif() {
		if(arene.getObjectif() != null){
			GraphicsContext gc = getGraphicsContext2D();
			Objectif obj = arene.getObjectif();
			double w = Constantes.OBJ_RADIUS * getWidth() / Constantes.L;
			double h = Constantes.OBJ_RADIUS * getHeight() / Constantes.H;
			double x = convertX(obj.getX());
			double y = convertY(obj.getY());
			gc.setFill(Color.SPRINGGREEN);
			gc.fillOval(x-(w/2), y-(h/2), w, h);
		}
	}

	private void drawObstacles() {
		GraphicsContext gc = getGraphicsContext2D();
		gc.setFill(Color.SANDYBROWN);
		for(Obstacle o : arene.getObstacles()) {
			double w = Constantes.OB_RADIUS * getWidth() / Constantes.L;
			double h = Constantes.OB_RADIUS * getHeight() / Constantes.H;
			double x = convertX(o.getX());
			double y = convertY(o.getY());
			gc.fillOval(x-(w/2), y-(h/2), w, h);
		}
	}

	public void drawWaitingScreen() {
		GraphicsContext gc = getGraphicsContext2D();
		gc.clearRect(0, 0, getWidth(), getHeight());
		drawLimite();
		gc.setTextAlign(TextAlignment.CENTER);
		gc.setTextBaseline(VPos.CENTER);
		gc.setFill(Color.WHITE);
		gc.fillText(arene.getLog(), getWidth()/2, getHeight()/2);
	}

	public void draw() {
		getGraphicsContext2D().clearRect(0, 0, getWidth(), getHeight());
		drawVehicules();
		drawObjectif();
		drawObstacles();
		drawLimite();
	}
}
