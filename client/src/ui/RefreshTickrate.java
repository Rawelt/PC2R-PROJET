package ui;

import elements.Arene;
import elements.Constantes;
import elements.Phase;

public class RefreshTickrate extends Thread{
	private JeuCanvas jeu_canvas;
	private Arene arene;

	public RefreshTickrate(JeuCanvas canvas, Arene arene) {
		this.jeu_canvas = canvas;
		this.arene = arene;
	}

	//Redessine le canvas du jeu
	public void run() {
		try {
			while(true) {
				synchronized (arene) {
					while(arene.getPhase() == Phase.ATTENTE) {
						jeu_canvas.drawWaitingScreen();
						arene.wait();
					}
					arene.moveAll();
					jeu_canvas.draw();
				}
				Thread.sleep(1000/Constantes.REFRESH_TICKRATE);
			}
		}catch(Exception e) {
			System.out.println("RefreshTickrate : " + e);
		}
	}
}
