package client;

import elements.Arene;
import elements.Constantes;
import elements.Phase;

public class ServerTickrate extends Thread{
	private Arene arene;

	public ServerTickrate(Arene arene) {
		this.arene = arene;
	}

	//Envoie les commandes de l'utilisateurs au serveur
	public void run() {
		try {
			while(true) {
				synchronized (arene) {
					while(arene.getPhase() == Phase.ATTENTE) {
						arene.wait();
					}
					if(arene.getA() != 0 || arene.getT() != 0) {
						arene.getOutChan().print("NEWCOM/A"+arene.getA()+"T"+arene.getT()+"/\n");
						arene.getOutChan().flush();
						arene.setA(0);
						arene.setT(0);
					}
				}
				Thread.sleep(1000/Constantes.SERVER_TICKRATE);
			}
		}catch(Exception e) {
			System.out.println("ServerTickrate :" + e);
		}
	}
}
