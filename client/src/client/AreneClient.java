package client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import elements.Arene;
import javafx.application.Application;
import javafx.stage.Stage;
import ui.AreneUI;

public class AreneClient extends Application{
	private static int port;
	private static String hote;

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("<hote> <port>");
			System.exit(1);
		}
		port = Integer.parseInt(args[1]);
		hote = args[0];
		launch(args);
	}

	public void start(Stage stg){
		try {
			Socket s = new Socket(hote, port);
			BufferedReader inchan = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter outchan = new PrintWriter(s.getOutputStream(),true);
			Arene arene = new Arene(inchan,outchan);
			new AreneUI(arene).start(stg);
		}catch(Exception e) {
			System.out.println("AreneClient : "+e);
			System.exit(1);
		}
	}
}
