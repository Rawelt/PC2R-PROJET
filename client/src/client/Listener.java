package client;

import elements.Arene;
import ui.SideBar;

public class Listener extends Thread {
	private Arene arene;
	private SideBar sidebar;

	public Listener(Arene arene,SideBar sidebar) {
		this.arene = arene;
		this.sidebar = sidebar;
	}

	//Ecoute les messages du serveurs et les traite
	public void run() {
		try {
			String msg;
			while(true) {
				msg = arene.getInChan().readLine();
				Instruction.parse(arene,msg,sidebar);
			}
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}
