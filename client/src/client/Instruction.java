package client;

import java.util.Map;

import elements.Arene;
import elements.Phase;
import javafx.application.Platform;
import ui.SideBar;

public class Instruction {

	//======================
	//FONCTIONS AUXILLIAIRES
	//======================

	//getX("X0.000125Y-2.5VX0.15VY1.2A1.5") => 0.000125
	//getX("X0.000125Y-2.5") => 0.000125
	private static double getX(String vcoord) {
		String[] tmp = vcoord.split("Y");
		return Double.parseDouble(tmp[0].substring(1, tmp[0].length()));
	}

	//getY("X0.000125Y-2.5VX0.15VY1.2A1.5") => -2.5
	//getY("X0.000125Y-2.5") => -2.5
	private static double getY(String coord) {
		String[] tmp = coord.split("Y");
		tmp = tmp[1].split("VX");
		return Double.parseDouble(tmp[0]);
	}

	//getVX("X0.000125Y-2.5VX0.15VY1.2A1.5") => 0.15
	private static double getVX(String vcoord) {
		String[] tmp = vcoord.split("VX");
		tmp = tmp[1].split("VY");
		return Double.parseDouble(tmp[0]);
	}

	//getVY("X0.000125Y-2.5VX0.15VY1.2A1.5") => 1.2
	private static double getVY(String vcoord) {
		String[] tmp = vcoord.split("VY");
		tmp = tmp[1].split("A");
		return Double.parseDouble(tmp[0]);
	}

	//getA("0.15VY1.2A1.5") => 1.5
	private static double getA(String coord) {
		String[] tmp = coord.split("A");
		return Double.parseDouble(tmp[1]);
	}

	//parseVCoords(a,"riri:X0.000125Y-2.5VX-0.002VY0T1.57|fifi:X0Y0VX0VY0T0")
	//met les parametres des vechiules a jour
	private static void parseVCoords(Arene arene,String vcoords) {
		String[] li = vcoords.split("\\|");
		for(int i=0;i<li.length;i++) {
			String[] user_coord = li[i].split(":");
			double x = getX(user_coord[1]);
			double y = getY(user_coord[1]);
			double vx = getVX(user_coord[1]);
			double vy = getVY(user_coord[1]);
			double angle = getA(user_coord[1]);
			if(arene.getMonVehicule() == null && user_coord[0].equals(arene.getMonNom())) {
				arene.setMonVehicule(x,y,vx,vy,angle);
			}else {
				arene.setVehicule(user_coord[0], x, y, vx, vy, angle);
			}
		}
	}

	//parseCoords(a,"riri:1|fifi:2|loulou:14")
	//met les scores des joueurs a jour
	private static void parseScores(Arene arene,String scores) {
		String[] li = scores.split("\\|");
		for(int i=0;i<li.length;i++) {
			String[] user_score = li[i].split(":");
			arene.setScoreForVehicule(user_score[0], Integer.parseInt(user_score[1]));
		}
	}

	//parseOCoords(a,X0.000125Y-2.5|X0Y0")
	//ajoute les obstacles de l'arene
	private static void parseOCoords(Arene arene,String ocoords) {
		if (arene.getObstacles().isEmpty()) {
			String[] li = ocoords.split("\\|");
			for(int i=0;i<li.length;i++) {
				double x = getX(li[i]);
				double y = getY(li[i]);
				arene.setObstacle(x, y);
			}
		}
	}

	//Met les scores dans le log de l'arene pour pouvoir les afficher
	private static void setEndScreen(Arene arene) {
		StringBuilder sb = new StringBuilder();
		sb.append("Fin de partie\n\n");
		for(Map.Entry<String, Integer> entry : arene.getScores().entrySet()) {
			sb.append(entry.getKey() + " : " + entry.getValue() + "\n");
		}
		sb.append("\nUne nouvelle partie va bientôt démarrer");
		arene.setLog(sb.toString());
	}

	//=============================================
	//Fonctions pour la partie graphique de sidebar
	//=============================================

	//Update les scores affiches
	private static void updateScores(SideBar sidebar,String scores) {
		Platform.runLater( () -> {
			sidebar.setScores(scores);
		});
	}

	//Ajoute un nouveau joueur dans les scores
	private static void addScore(SideBar sidebar, String nom) {
		Platform.runLater( () -> {
			sidebar.newPlayer(nom);
		});
	}

	//Enleve le score du joueur parti
	private static void removeScore(SideBar sidebar, String nom) {
		Platform.runLater( () -> {
			sidebar.playerLeft(nom);
		});
	}

	//Enleve le score du joueur parti
	private static void resetScores(SideBar sidebar) {
		Platform.runLater( () -> {
			sidebar.reset();
		});
	}

	//Ajoute un nouveau message au chat
	private static void addNewMessage(SideBar sidebar, String source) {
		String message = source.substring(source.indexOf('/')+1,source.length()-1);
		Platform.runLater( () -> {
			sidebar.reception(message);
		});
	}

	//Ajoute un message prive au chat
	private static void addPrivateMessage(SideBar sidebar, String[] li) {
		StringBuilder message = new StringBuilder();
		for(int i=1;i<li.length-2;i++) {
			message.append(li[i] + "/");
		}
		if(li.length>2) {
			message.append(li[li.length-2]);
		}
		Platform.runLater( () -> {
			sidebar.preception(message.toString());
		});
	}


	//==================================
	//TRAITEMENT DES MESSAGES DU SERVEUR
	//==================================

	private static void welcomeInstr(Arene arene, String[] li) {
		double x = getX(li[3]);
		double y = getY(li[3]);
		synchronized(arene) {
			parseScores(arene, li[2]);
			arene.setObjectif(x, y);
			if(li[1].equals("jeu")) {
				arene.setPhase(Phase.JEU);
			}else {
				arene.setPhase(Phase.ATTENTE);
			}
			parseOCoords(arene, li[4]);
		}
	}

	private static void newplayerInstr(Arene arene, String[] li ,SideBar sidebar) {
		synchronized(arene) {
			arene.setScoreForVehicule(li[1], 0);
		}
		Platform.runLater( () -> {
			sidebar.reception(li[1] + " s'est connecté");
		});
	}

	private static void playerleftInstr(Arene arene, String[] li, SideBar sidebar) {
		synchronized(arene) {
			arene.removeJoueur(li[1]);
		}
		Platform.runLater( () -> {
			sidebar.reception(li[1] + " s'est déconnecté");
		});
	}

	private static void sessionInstr(Arene arene, String[] li) {
		double x = getX(li[2]);
		double y = getY(li[2]);
		synchronized(arene) {
			parseVCoords(arene, li[1]);
			arene.setObjectif(x, y);
			arene.setPhase(Phase.JEU);
			arene.notifyAll();
			parseOCoords(arene, li[3]);
		}
	}

	private static void winnerInstr(Arene arene, String[] li) {
		synchronized(arene) {
			parseScores(arene, li[1]);
			setEndScreen(arene);
			arene.reset_partie();
		}
	}

	private static void tickInstr(Arene arene, String[] li) {
		synchronized(arene) {
			parseVCoords(arene, li[1]);
		}
	}

	private static void newobjInstr(Arene arene, String[] li) {
		double x = getX(li[1]);
		double y = getY(li[1]);
		synchronized(arene) {
			parseScores(arene, li[2]);
			arene.setObjectif(x, y);
		}
	}

	//==============================
	//PARSER DES MESSAGES DU SERVEUR
	//==============================
	public static boolean parse(Arene arene,String str,SideBar sidebar) {
		String[] li = str.split("/");
		switch(li[0]) {
		case "WELCOME" :
			welcomeInstr(arene, li);
			updateScores(sidebar,li[2]);
			return true;
		case "DENIED" :
			return false;
		case "NEWPLAYER" :
			newplayerInstr(arene, li, sidebar);
			addScore(sidebar, li[1]);
			return true;
		case "PLAYERLEFT" :
			playerleftInstr(arene, li, sidebar);
			removeScore(sidebar, li[1]);
			return true;
		case "SESSION" :
			sessionInstr(arene, li);
			resetScores(sidebar);
			return true;
		case "WINNER" :
			winnerInstr(arene, li);
			updateScores(sidebar,li[1]);
			return true;
		case "TICK" :
			tickInstr(arene, li);
			return true;
		case "NEWOBJ" :
			newobjInstr(arene, li);
			updateScores(sidebar,li[2]);
			return true;
		case "RECEPTION" :
			addNewMessage(sidebar,str);
			return true;
		case "PRECEPTION" :
			addPrivateMessage(sidebar,li);
			return true;
		default:
			System.out.println(str);
			return false;
		}
	}
}
