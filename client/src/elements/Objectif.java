package elements;

public class Objectif {
	private double x;
	private double y;

	public Objectif(double x, double y) {
		this.x = Constantes.getTrueX(x);
		this.y = Constantes.getTrueY(y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public boolean isOnObjectif(Vehicule v) {
		return Math.sqrt(Math.pow(x - v.getX(), 2) + Math.pow(y - v.getY(), 2)) <= Constantes.OBJ_RADIUS;
	}
}
