package elements;

public class Obstacle {
	private double x;
	private double y;

	public Obstacle(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public boolean isInObstacle(double x, double y) {
		return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2)) <= Constantes.OB_RADIUS;
	}
}
