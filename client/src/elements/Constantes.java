package elements;

public class Constantes {
	public static double L = 1;
	public static double H = 1;
	public static double TURNIT = Math.PI/12;
	public static double THRUSTIT = 0.001;
	public static int REFRESH_TICKRATE = 45;
	public static int SERVER_TICKRATE = 30;
	public static double OBJ_RADIUS = 0.05;
	public static double VITESSE_LIMITE = 0.0001;
	public static double OB_RADIUS = 0.1;
	public static double VE_RADIUS = 0.05;

	//Retourne la coordonnee x respectant le monde torique
	public static double getTrueX(double x) {
		double res = x;
		if(res>L) {
			if(res%(2*L) > L) {
				res = res%(2*L) - 2*L;
			}else {
				res = res%(2*L);
			}
		}
		if(res<= -L) {
			if(res%(2*L) <= -L) {
				res = res%(2*L) + 2*L;
			}else {
				res = res%(2*L);
			}
		}
		return res;
	}

	//Retourne la coordonnee y respectant le monde torique
	public static double getTrueY(double y) {
		double res = y;
		if(res>H) {
			if(res%(2*H) > H) {
				res = res%(2*H) - 2*H;
			}else {
				res = res%(2*H);
			}
		}
		if(res<= -H) {
			if(res%(2*H) <= -H) {
				res = res%(2*H) + 2*H;
			}else {
				res = res%(2*H);
			}
		}
		return res;
	}
}
