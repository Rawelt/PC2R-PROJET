package elements;

public class Vehicule {
	private String nom;
	private double x,y;
	private double vx,vy;
	private double angle;

	public Vehicule(String nom, double x,double y, double vx, double vy, double angle) {
		this.nom = nom;
		this.x = Constantes.getTrueX(x);
		this.y = Constantes.getTrueY(y);
		this.vx = vx;
		this.vy = vy;
		this.angle = angle%(2*Math.PI);
	}

	public String getNom() {
		return nom;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getVx() {
		return vx;
	}

	public double getVy() {
		return vy;
	}

	public double getAngle() {
		return angle;
	}

	public void setAllParametres(double x, double y, double vx, double vy, double angle) {
		this.x = Constantes.getTrueX(x);
		this.y = Constantes.getTrueY(y);
		this.vx = vx;
		this.vy = vy;
		this.angle = angle%(2*Math.PI);
		//		limiteVitesse();
	}

	//	private void limiteVitesse() {
	//		if(vx*vx + vy*vy > Constantes.VITESSE_LIMITE) {
	//			if(vy != 0) {
	//				boolean vx_negatif = vx < 0;
	//				boolean vy_negatif = vy < 0;
	//				double k = Math.abs(vx/vy);
	//				double new_vx = Math.sqrt(Constantes.VITESSE_LIMITE/(k*k + 1.)) * k;
	//				double new_vy = Math.sqrt(Constantes.VITESSE_LIMITE/(k*k + 1.));
	//				if (vx_negatif) {
	//					new_vx = - new_vx;
	//				}
	//				if(vy_negatif) {
	//					new_vy = - new_vy;
	//				}
	//				vx = new_vx;
	//				vy = new_vy;
	//			}else {
	//				vx = Math.sqrt(Constantes.VITESSE_LIMITE);
	//			}
	//		}
	//	}

	public void inverseVitesse() {
		vx = -vx;
		vy = -vy;
	}

	public void move() {
		x = Constantes.getTrueX(x+vx);
		y = Constantes.getTrueY(y+vy);
	}

	public boolean isInVehicule(double x, double y) {
		return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2)) <= Constantes.VE_RADIUS;
	}
}
