package elements;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Arene {
	private Map<String,Vehicule> all_vehicules;
	private Map<String,Integer> scores;
	private Set<Obstacle> all_obstacles;
	private Vehicule mon_vehicule;
	private String mon_nom;
	private Objectif obj;
	private Phase phase;
	private String log;

	private int T;
	private double A;

	private BufferedReader inchan;
	private PrintWriter outchan;

	public Arene(BufferedReader inchan,PrintWriter outchan) {
		all_vehicules = new HashMap<String,Vehicule>();
		scores = new HashMap<String,Integer>();
		all_obstacles = new HashSet<Obstacle>();
		mon_vehicule = null;
		mon_nom = null;
		obj = null;
		phase = Phase.ATTENTE;
		log = "Une nouvelle partie va bientôt démarrer";
		T = 0;
		A = 0;

		this.inchan = inchan;
		this.outchan = outchan;
	}

	//======
	//GETTER
	//======

	public BufferedReader getInChan() {
		return inchan;
	}

	public PrintWriter getOutChan() {
		return outchan;
	}

	public Map<String,Vehicule> getAllVehicules(){
		return all_vehicules;
	}

	public Map<String,Integer> getScores() {
		return scores;
	}

	public Vehicule getMonVehicule() {
		return mon_vehicule;
	}

	public String getMonNom() {
		return mon_nom;
	}

	public Phase getPhase() {
		return phase;
	}

	public Objectif getObjectif() {
		return obj;
	}

	public Set<Obstacle> getObstacles(){
		return all_obstacles;
	}

	public String getLog() {
		return log;
	}

	public int getT() {
		return T;
	}

	public double getA() {
		return A;
	}


	//======
	//SETTER
	//======

	public void setMonVehicule(double x, double y, double vx, double vy, double angle) {
		mon_vehicule = new Vehicule(mon_nom, x, y, vx, vy, angle);
		all_vehicules.put(mon_nom, mon_vehicule);
	}

	public void setVehicule(String nom, double x, double y, double vx, double vy, double angle) {
		Vehicule v = all_vehicules.get(nom);
		if(v == null) {
			v = new Vehicule(nom, x, y, vx, vy, angle);
			all_vehicules.put(nom, v);
		}else {
			v.setAllParametres(x, y, vx, vy, angle);
		}
	}

	public void setScoreForVehicule(String nom, int score) {
		scores.put(nom, score);
	}

	public void setMonNom(String nom) {
		mon_nom = nom;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	public void setObjectif(double x, double y) {
		if(obj == null) {
			obj = new Objectif(x, y);
		}else {
			obj.setPosition(x, y);
		}
	}

	public void setObstacle(double x,double y) {
		all_obstacles.add(new Obstacle(x, y));
	}

	public void setLog(String log) {
		this.log = log;
	}

	public void setT(int v) {
		T = v;
	}

	public void setA(double val) {
		A = val%(2*Math.PI);
	}


	//=====
	//TESTS
	//=====

	//Verifie si la position (x,y) se touche un vehicule de l'arene
	private boolean isInVehicule(double x, double y) {
		for(Vehicule v : all_vehicules.values()) {
			if(v.isInVehicule(x, y)) {
				return true;
			}
		}
		return false;
	}

	//Verifie si la position (x,y) se trouve dans un obstacle
	private boolean isInObstacle(double x, double y) {
		for(Obstacle o : all_obstacles) {
			if(o.isInObstacle(x, y)){
				return true;
			}
		}
		return false;
	}


	//======
	//AUTRES
	//======

	//Change la position de tous les vehicules pendant le tick
	public void moveAll() {
		for(Vehicule v : all_vehicules.values()) {
			if(isInObstacle(v.getX(), v.getY()) || isInVehicule(v.getX(), v.getY())) {
				v.inverseVitesse();
			}
			v.move();
		}
	}

	public void removeJoueur(String nom) {
		scores.remove(nom);
		all_vehicules.remove(nom);
	}

	public void reset_partie() {
		all_vehicules.clear();
		for(String nom : scores.keySet()) {
			scores.put(nom, 0);
		}
		all_obstacles.clear();
		mon_vehicule = null;
		obj = null;
		phase = Phase.ATTENTE;
		A = 0;
		T = 0;
	}
}